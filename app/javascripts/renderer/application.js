jQuery = $ = require('jquery');
require('electron').ipcRenderer.on('loaded', function(event, data) {});

var socket = io.connect('http://localhost:3000');

toggleVideo = function() {
    $('video').each(function() {
        $(this).toggle();
        $(this)[0].currentTime = 0;
        if ($(this)[0].paused) {
            $(this)[0].play();
        } else {
            $(this)[0].pause();
        }
    });
};

// require('electron').ipcRenderer.on('toggle', function() {
//     toggleVideo();
// });

socket.on('toggle', function (msg){
    toggleVideo();
    console.log('message: ' + msg);
});
