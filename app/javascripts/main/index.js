var electron, path, json;

path = require('path');
json = require('../../package.json');

electron = require('electron');

electron.app.on('ready', function() {
    var window;

    window = new electron.BrowserWindow({
        title: json.name,
        // width: json.settings.width,
        // height: json.settings.height
        fullscreen: true
    });

    window.loadURL('file://' + path.join(__dirname, '..', '..') + '/index.html');

    window.webContents.on('did-finish-load', function() {
        
    });

    window.on('closed', function() {
        window = null;
    });

});
