var serialport = require('serialport');
SerialPort = serialport.SerialPort;
var port_name = 'FTDI';
var init_data = 0;
var flag = 0;

serialport.list(function(err, ports) {
    console.log(ports);
    ports.forEach(function(com) {
        if (com.manufacturer === port_name) {
            connect(com.comName);
        }
    });
});

function connect(port_c) {
    var app = require('express')();
    var http = require('http').Server(app);
    var io = require('socket.io')(http);


    console.log('porta: ' + port_c);

    port = new SerialPort(port_c, {
        baudrate: 9600,
        autooOpen: false,
        parser: serialport.parsers.readline('\n')
    });

    console.log('open');

    io.on('connection', function(socket) {
        port.on('data', function(data) {
            console.log('toggle');
            socket.emit('toggle');
        });
        console.log('a user connected');
    });

    http.listen(3000, function() {
        console.log('listening on *:3000');
    });

}
