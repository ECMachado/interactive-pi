var path = require('path');

module.exports = {
  appPath: function() {
    switch (process.platform) {
      case 'darwin':
        return path.join(__dirname, '..', '.tmp', 'osx', 'InteractivePiBozon.app', 'Contents', 'MacOS', 'InteractivePiBozon');
      case 'linux':
        return path.join(__dirname, '..', '.tmp', 'linux', 'InteractivePiBozon');
      default:
        throw 'Unsupported platform';
    }
  }
};
